package com.jtew.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.jtew.common.AcademyAward;

@Service
public class FileReaderService {

  private static final String REGEX = "([^\"]+)";
  private static final Pattern PATTERN = Pattern.compile(REGEX);

  public Collection<AcademyAward> getAcademyAwardFromFile() {
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource("academy_award_actresses.csv").getFile());

    Collection<AcademyAward> academyAwards = new ArrayList<>();
    try {
      Scanner scanner = new Scanner(file);
      while (scanner.hasNextLine()) {
        academyAwards.add(buildAcademyAward(scanner.nextLine()));
      }
    }
    catch (FileNotFoundException e) {
      e.printStackTrace();
    }

    return academyAwards;
  }

  private AcademyAward buildAcademyAward(String str) {
    String[] stringArray = str.split(",");
    LinkedList<String> cleansedStrings = cleanseStringArray(stringArray);

    AcademyAward academyAward = new AcademyAward();
    try {
      academyAward.setYear(cleansedStrings.get(0));
      academyAward.setActress(cleansedStrings.get(1));
      academyAward.setMovie(cleansedStrings.get(2));
    }
    catch (NullPointerException e) {
      e.printStackTrace();
    }
    return academyAward;
  }

  private LinkedList<String> cleanseStringArray(String[] stringArray) {
    LinkedList<String> returnValues = new LinkedList<>();
    for (String s : stringArray) {
      Matcher matcher = PATTERN.matcher(s);
      while (matcher.find()) {
        returnValues.add(matcher.group());
      }
    }
    return returnValues;
  }
}
