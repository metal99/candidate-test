package com.jtew.common;

import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;

@AutoProperty
public class AcademyAward {

  private String year;
  private String actress;
  private String movie;

  public AcademyAward() {
  }

  public AcademyAward(String year, String movie, String actress) {
    this.year = year;
    this.movie = movie;
    this.actress = actress;
  }

  public String getYear() {
    return year;
  }

  public void setYear(String year) {
    this.year = year;
  }

  public String getMovie() {
    return movie;
  }

  public void setMovie(String movie) {
    this.movie = movie;
  }

  public String getActress() {
    return actress;
  }

  public void setActress(String actress) {
    this.actress = actress;
  }

  @Override
  public boolean equals(Object other) {
    return Pojomatic.equals(this, other);
  }

  @Override
  public int hashCode() {
    return Pojomatic.hashCode(this);
  }

  @Override
  public String toString() {
    return Pojomatic.toString(this);
  }
}
