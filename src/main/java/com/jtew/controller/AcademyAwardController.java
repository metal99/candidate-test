package com.jtew.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.jtew.common.AcademyAward;
import com.jtew.service.FileReaderService;

@RestController
@RequestMapping(AcademyAwardController.ACADEMYAWARD)
public class AcademyAwardController {

  private final FileReaderService fileReaderService;

  public static final String ACADEMYAWARD = "/academyaward";

  @Inject
  public AcademyAwardController(FileReaderService fileReaderService) {
    this.fileReaderService = fileReaderService;
  }

  @RequestMapping(
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  public String filesHelper() throws JsonProcessingException {
    Collection<AcademyAward> file = fileReaderService.getAcademyAwardFromFile();
    ObjectMapper mapper = new ObjectMapper();
    return mapper.writeValueAsString(file);
  }
}
